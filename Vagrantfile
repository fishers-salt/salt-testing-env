# -*- mode: ruby -*-
# vi: set ft=ruby :
# frozen_string_literal: true

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

$prepare_setup_script = <<~SCRIPT
  chmod u+x /home/vagrant/setup.sh
SCRIPT

$write_gpg_keys = <<~SCRIPT
  apt-get install -y python3-gnupg
  apt-get install -y rng-tools
  sudo mkdir -p /etc/salt/gpgkeys
  sudo chown -R root:root /etc/salt/gpgkeys
  sudo chmod 700 /etc/salt/gpgkeys
  echo "$1" > private.gpg.key
  echo "$2" > public.gpg.key
  sudo gpg --homedir /etc/salt/gpgkeys --import /home/vagrant/private.gpg.key
  echo -e "5\ny\n" | sudo gpg --homedir /etc/salt/gpgkeys --command-fd 0 --expert --edit-key 6C25A0D40D53675E trust
  sudo gpg --import /home/vagrant/public.gpg.key
  rm private.gpg.key
  rm public.gpg.key
  echo "$3" > private.ssh.key
  echo "$4" > public.ssh.key
  sudo mkdir -p /root/.ssh
  sudo mv /home/vagrant/private.ssh.key /root/.ssh/id_ed25519
  sudo mv /home/vagrant/public.ssh.key /root/.ssh/id_ed25519.pub
  sudo mv /home/vagrant/known_hosts /root/.ssh/known_hosts
  sudo chown -R root:root /root/.ssh
  sudo chmod 700 /root/.ssh
  sudo chmod 600 /root/.ssh/id_ed25519
  sudo chmod 600 /root/.ssh/id_ed25519.pub
  sudo chmod 600 /root/.ssh/known_hosts
  echo 'printf "Now run the setup.sh script to finish installation"' >> /home/vagrant/.bashrc
SCRIPT

SKIP_STATES = %w[.tool-versions fishers-salt-states fishers-salt-pillar . ..].freeze
STATES_DIR = "#{Dir.home}/code/salt/personal-formulas"
EXTERNALS_DIR = "#{Dir.home}/code/salt/external-formulas"

def write_master_config(filename)
  File.open(filename, 'w') do |file|
    conf_start = <<~CONF
      fileserver_backend:
        - roots

      file_roots:
        base:
          - /srv/salt
          - /srv/fishers-salt-formula
    CONF
    file.puts(conf_start)
    Dir.foreach(STATES_DIR) do |dir|
      next if SKIP_STATES.include?(dir)

      file.puts("    - /srv/#{dir}")
    end
    Dir.foreach(EXTERNALS_DIR.to_s) do |dir|
      next if SKIP_STATES.include?(dir)

      file.puts("    - /srv/#{dir}")
    end
    conf_end = <<~CONF

      pillar_roots:
        base:
          - /srv/pillar
    CONF
    file.puts(conf_end)
  end
end

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  master_conf_file = 'saltstack/etc/initial-master.conf'
  write_master_config(master_conf_file)
  os = 'generic/ubuntu2204'
  net_ip = '192.168.56'

  config.vm.define :master, primary: true do |master_config|
    master_config.vm.provider 'virtualbox' do |master|
      master.memory = '2048'
      master.cpus = 1
      master.name = 'saltmaster'
    end

    master_config.vm.box = os.to_s
    master_config.vm.host_name = 'saltmaster.local'
    master_config.vm.network 'private_network', ip: "#{net_ip}.10"
    master_config.vm.synced_folder "#{STATES_DIR}/fishers-salt-states", '/srv/fishers-salt-states'
    master_config.vm.synced_folder "#{STATES_DIR}/fishers-salt-pillar", '/srv/pillar'
    Dir.foreach(STATES_DIR) do |dir|
      next if SKIP_STATES.include?(dir)

      master_config.vm.synced_folder "#{STATES_DIR}/#{dir}", "/srv/#{dir}"
    end
    Dir.foreach(EXTERNALS_DIR.to_s) do |dir|
      next if SKIP_STATES.include?(dir)

      master_config.vm.synced_folder "#{EXTERNALS_DIR}/#{dir}", "/srv/#{dir}"
    end
    master_config.vm.provision :salt do |salt|
      salt.master_config = master_conf_file.to_s
      salt.install_type = 'stable'
      salt.install_master = true
      salt.verbose = true
      salt.colorize = true
      # Workaround for https://github.com/saltstack/salt-bootstrap/issues/1919
      salt.version = 3005
      salt.bootstrap_options = '-M -P -A localhost -i saltmaster'
    end
    master_config.vm.provision :file, source: 'saltstack/scripts/setup.sh', destination: 'setup.sh'
    master_config.vm.provision :file, source: 'saltstack/ssh/known_hosts', destination: 'known_hosts'
    master_config.vm.provision :shell, inline: $prepare_setup_script
    master_config.vm.provision :shell do |keys|
      keys.inline = $write_gpg_keys
      keys.args = [
        `pass salt/vagrant/experiment-salt-master_gpg-private.key`,
        `pass salt/vagrant/experiment-salt-master_gpg-public.key`,
        `pass salt/vagrant/ssh_key`,
        `pass salt/vagrant/ssh_key.pub`
      ]
    end
  end

  [
    ['saltminion1',    "#{net_ip}.11",    '1024',    os],
    ['saltminion2',    "#{net_ip}.12",    '1024',    os]
  ].each do |vmname, ip, mem, osname|
    config.vm.define vmname.to_s do |minion_config|
      minion_config.vm.provider 'virtualbox' do |minion|
        minion.memory = mem.to_s
        minion.cpus = 1
        minion.name = vmname.to_s
      end

      minion_config.vm.box = osname.to_s
      minion_config.vm.hostname = vmname.to_s
      minion_config.vm.network 'private_network', ip: ip.to_s

      minion_config.vm.provision :salt do |salt|
        salt.minion_config = "saltstack/etc/#{vmname}.conf"
        salt.install_type = 'stable'
        # Workaround for https://github.com/saltstack/salt-bootstrap/issues/1919
        salt.version = 3005
        salt.verbose = true
        salt.colorize = true
        salt.bootstrap_options = '-P -c /tmp -x python3'
      end
    end
  end
end
