#!/bin/bash

DELAY=10

sed --in-place '/Now run the setup.sh script/d' "$HOME/.bashrc"
sleep $DELAY
sudo salt-key --accept-all --yes
sleep $DELAY
sudo salt saltmaster grains.set role salt_master
sleep $DELAY
sudo salt saltmaster grains.set realm vagrant
sleep $DELAY
sudo systemctl restart salt-minion
sleep $DELAY
sudo systemctl restart salt-master
sleep $DELAY
sudo salt-run fileserver.update
sleep $DELAY
sudo salt saltmaster saltutil.refresh_pillar
sleep $DELAY
sudo salt saltmaster state.apply salt.minion
sleep $DELAY
sudo salt saltmaster state.apply salt.master
sleep $DELAY
sudo salt saltmaster state.apply salt.master
sleep $DELAY
sudo salt saltmaster state.apply saltplusplus
